﻿#Requires -RunAsAdministrator

Import-Module (Join-Path $PSScriptRoot Utils.psm1)

$result = Assert-HyperV

if ($result) {
    'Hyper-V is already installed!'
}
else {
    Write-Output 'Hyper-V is not installed, installing it'
    Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Hyper-V -All
}

Write-Output Done!