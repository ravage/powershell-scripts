function New-InfraContainer()
{
    cd c:\k
    $version = "microsoft/nanoserver"
    "FROM $($version)" | Out-File -encoding ascii -FilePath Dockerfile
    "CMD cmd /c ping -t localhost" | Out-File -encoding ascii -FilePath Dockerfile -Append
    docker build -t kubletwin/pause .
}

New-InfraContainer