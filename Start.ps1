$info = New-Object System.Diagnostics.ProcessStartInfo
$info.FileName = ""
$info.Arguments = "-f $env:SDP -v $env:STREAM_PATH"
$info.UseShellExecute = $false
$info.RedirectStandardError = $false
$info.RedirectStandardInput = $false
$info.RedirectStandardOutput = $false
$info.CreateNoWindow = $true
$info.ErrorDialog = $false

$process= New-Object System.Diagnostics.Process
$process.StartInfo = $info

# Start the process
$process.Start()
#$process.StandardOutput.ReadToEnd();
#$process.StandardError.ReadToEnd();
$process.WaitForExit()