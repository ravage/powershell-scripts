﻿[CmdletBinding()]
Param(
    [Parameter(Mandatory=$True)]
    [string]$address
)
$cred = Get-Credential
Enter-PSSession -ComputerName $address -credential $cred