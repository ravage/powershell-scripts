﻿function Assert-HyperV {
    return Assert-WindowsOptionalFeature('Microsoft-Hyper-V')
}

function Assert-Containers {
    return Assert-WindowsOptionalFeature('Containers')
}

function Assert-WindowsOptionalFeature {
    param([string] $feature)

    $result = Get-WindowsOptionalFeature -Online -FeatureName $feature
    return $result.State -eq 'Enabled'
}

function Invoke-Download {
    param (
        [string] $source,
        [string] $destination
    )

    (New-Object System.Net.WebClient).DownloadFile($source, $destination)
}

function LogInfo {
    param (
        [string] $msg
    )

    Write-Host $msg -ForegroundColor Green
}

Export-ModuleMember -Function *
