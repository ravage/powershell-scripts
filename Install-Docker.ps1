﻿#Requires -RunAsAdministrator

Import-Module (Join-Path $PSScriptRoot Utils.psm1)

$sourceUrl = "https://download.docker.com/components/engine/windows-server/18.03/docker-18.03.1-ee-3.zip"

$service = 'Docker'
$serviceExists = Get-WmiObject -Class Win32_Service -Filter "Name='$service'"
$destination = Join-Path $env:TEMP docker.zip

function Set-Path {
  param([string] $path)

  $env:path += ";$env:ProgramFiles\docker"
  $newPath = "$env:ProgramFiles\docker;" + [Environment]::GetEnvironmentVariable("PATH", [EnvironmentVariableTarget]::Machine)
  [Environment]::SetEnvironmentVariable("PATH", $newPath, [EnvironmentVariableTarget]::Machine)
}

if (Assert-HyperV -and Assert-Containers) {
  Write-Output 'Features: Hyper-V and Containers Already Installed'
} else {
  Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Hyper-V,Containers -All
}

Write-Output "Downloading Docker Version $version"
Invoke-Download -source $sourceUrl -destination $destination

if ($serviceExists) {
    Write-Host 'Stopping and Unregistering Docker Service'
    Stop-Service $service
    dockerd --unregister-service
}

if (!$env:Path.Contains('docker')) {
  Set-Path("$env:ProgramFiles\docker")
}

Write-Output "Installing Docker"

Expand-Archive -Path $destination -DestinationPath $env:ProgramFiles -Force
Remove-Item -Force $destination

dockerd -H npipe:// -H 0.0.0.0:2375 --register-service

Start-Service $service
