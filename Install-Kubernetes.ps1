$version = 'v1.8.13'
$sourceUrl = "https://dl.k8s.io/$version/kubernetes-node-windows-amd64.tar.gz"
$destination = Join-Path $env:TEMP 'kubernetes-node-windows-amd64.tar.gz'
$kDir = 'c:\k'

function Invoke-Download {
    param (
        [string] $source,
        [string] $destination
    )

    (New-Object System.Net.WebClient).DownloadFile($source, $destination)
}

function LogInfo {
    param (
        [string] $msg
    )

    Write-Host $msg -ForegroundColor Green
}

function Expand-Tar($tarFile, $dest) {

    if (-not (Get-Command Expand-7Zip -ErrorAction Ignore)) {
        Install-Package -Scope CurrentUser -Force 7Zip4PowerShell > $null
    }

    Expand-7Zip $tarFile $dest
}

pushd $env:TEMP

LogInfo -msg "* Downloading Kubernetes $version..."
Invoke-Download -source $sourceUrl -destination $destination

LogInfo -msg "* Extracting..."
Expand-Tar $destination .
Expand-Tar kubernetes-node-windows-amd64.tar .
# tar -xzvf $destination *> $null


if(!(Test-Path -Path $kDir)){
    mkdir -p $kDir\cni\config *> $null
}

cp kubernetes\node\bin\*.exe c:\k

LogInfo -msg "* Cleanup..."
rm -r kubernetes
rm -r $destination

cd $kDir