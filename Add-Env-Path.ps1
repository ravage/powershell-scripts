﻿[CmdletBinding()]
Param(
    [Parameter(Mandatory=$True)]
    [string]$path
)

$old = (Get-ItemProperty -Path 'Registry::HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Session Manager\Environment\' -Name Path).Path

if (!(Test-Path $path)) {
    Write-Error "Non existing directory: $path"
    exit
}

if ($env:Path | Select-String -SimpleMatch $path) {
    Write-Error "($path) already in path"
    exit
}

$newPath = $old + ’;’ + $path

Set-ItemProperty -Path 'Registry::HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Session Manager\Environment\' -Name Path -Value $newPath

if ($env:Path.EndsWith(';')) {
    [System.Environment]::SetEnvironmentVariable('path', $path)
} else {
    [System.Environment]::SetEnvironmentVariable('path', ';' + $path)
}