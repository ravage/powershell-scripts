$version = 'v7.6.0.0p1-Beta'

function Invoke-Download {
  param (
         [string] $source,
         [string] $destination
         )

  (New-Object System.Net.WebClient).DownloadFile($source, $destination)
}

function Install-SSHD {
  param (
         [string] $version
         )

  [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

  $sourceUrl = "https://github.com/PowerShell/Win32-OpenSSH/releases/download/$version/OpenSSH-Win64.zip"
  $destination = Join-Path $env:TEMP OpenSSH-Win64.zip

  try {
    Get-Service sshd -ErrorAction Stop
    Stop-Service sshd
    Stop-Service ssh-agent
    Get-NetFirewallRule | ? Name -eq 'sshd' | Remove-NetFirewallRule
  }
  catch {
    Write-Verbose 'Service does not exist, continuing'
  }

  Invoke-Download -source $sourceUrl -destination $destination
  Expand-Archive -Path $destination -DestinationPath $env:ProgramFiles -Force
  Remove-Item -Force $destination

  cd 'C:\Program Files\OpenSSH-Win64\'

  .\install-sshd.ps1

  New-NetFirewallRule -Name sshd -DisplayName 'OpenSSH Server (sshd)' -Enabled True -Direction Inbound -Protocol TCP -Action Allow -LocalPort 22

  Start-Service sshd
  Start-Service ssh-agent

  Set-Service sshd -startupType automatic
  Set-Service ssh-agent -startupType automatic
}

Install-SSHD -version $version