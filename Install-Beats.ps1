#Requires -RunAsAdministrator

Import-Module (Join-Path $PSScriptRoot Utils.psm1)

$version = "6.2.4"
$sourceUrl = "https://artifacts.elastic.co/downloads/beats/metricbeat/metricbeat-$version-windows-x86_64.zip"
$destination = Join-Path $env:TEMP beats.zip

LogInfo -msg "Downloading Metricbeat Version $version"
Invoke-Download -source $sourceUrl -destination $destination
LogInfo -msg "Extracting..."
Expand-Archive -Path $destination -DestinationPath $env:ProgramFiles -Force
LogInfo -msg "Cleanup..."
Remove-Item -Force $destination