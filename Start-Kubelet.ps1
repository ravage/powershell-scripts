Param(
    [Parameter(Mandatory=$true)]
    $ClusterCIDR, # ="10.200.0.0/16",
    [Parameter(Mandatory=$true)]
    $NetworkMode, # = "transparent",
    [Parameter(Mandatory=$true)]
    $NetworkName, # = "k8s",
    [Parameter(Mandatory=$true)]
    $Interface, # = "Ethernet"
    [Parameter(Mandatory=$true)]
    $MainInterface # = "NIC1"
)

# Todo : Get these values using kubectl
$KubeDnsSuffix ="svc.cluster.local"
$KubeDnsServiceIp="8.8.8.8"
$ServiceCIDR="10.32.0.0/16"

$env:CONTAINER_NETWORK=$NetworkName

$WorkingDir = "c:\k"

function Get-PodGateway($podCIDR)
{
    return $podCIDR.substring(0,$podCIDR.lastIndexOf(".")) + ".1"
}

function Get-PodCIDR()
{
    $podCIDR=c:\k\kubectl.exe --kubeconfig=c:\k\config get nodes/$($(hostname).ToLower()) -o custom-columns=podCidr:.spec.podCIDR --no-headers
    return $podCIDR
}

$podCIDR = Get-PodCIDR
$podGW = Get-PodGateway $podCIDR

$dockerNetwork=docker network ls -f name=k8s

if (!($dockerNetwork -is [array] -and $dockerNetwork[1].Contains($NetworkName))) {
    docker network create -d $NetworkMode --subnet="$podCIDR" --gateway="$podGW" -o com.docker.network.windowsshim.interface="$Interface" -o com.docker.network.windowsshim.dnsservers="$KubeDnsServiceIp" -o com.docker.network.windowsshim.dnssuffix="$KubeDnsSuffix" k8s
}

Set-NetFirewallProfile -All -Enabled "false"
Disable-NetAdapterBinding -InterfaceAlias "vEthernet (HNSTransparent)" –ComponentID ms_tcpip6
#netsh int ipv4 set int "vEthernet (HNSTransparent)" for=en
#netsh int ipv4 set int $Interface for=en
#netsh int ipv4 set int $MainInterface for=en
netsh interface ipv4 set address "vEthernet (HNSTransparent)" static addr=$podGW mask=255.255.255.0
$viface = Get-NetIPInterface | Where InterfaceAlias -eq 'vEthernet (HNSTransparent)'
Set-NetIPInterface -InterfaceIndex $viface.InterfaceIndex -InterfaceMetric 15
Set-NetIPInterface -InterfaceIndex $viface.InterfaceIndex -Forwarding enabled
$miface = Get-NetIPInterface | Where InterfaceAlias -eq $MainInterface
Set-NetIPInterface -InterfaceIndex $miface.InterfaceIndex -Forwarding enabled

c:\k\kubelet.exe --hostname-override=$(hostname) --v=6 `
        --pod-infra-container-image="apprenda/pause" --resolv-conf="" `
        --allow-privileged=true --enable-debugging-handlers `
        --cluster-dns=$KubeDnsServiceIp --cluster-domain=$KubeDnsSuffix `
        --kubeconfig=c:\k\config --hairpin-mode=promiscuous-bridge `
        --image-pull-progress-deadline=20m --cgroups-per-qos=false `
        --enforce-node-allocatable=""