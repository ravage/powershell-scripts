﻿#Requires -RunAsAdministrator

Import-Module (Join-Path $PSScriptRoot Utils.psm1)

$result = Assert-Containers

if ($result) {
    'Containers feature is already installed!'
}
else {
    Write-Output 'Containers feature is not installed, installing it'
    Enable-WindowsOptionalFeature -Online -FeatureName Containers -All
}

Write-Output Done!