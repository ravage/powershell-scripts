$ifs =  Get-NetConnectionProfile

ForEach ($iface in $ifs) {
    # switch interfaces from Public to Private
    if ($iface.NetworkCategory -eq 'Public') {
        Set-NetConnectionProfile -InterfaceIndex $iface.InterfaceIndex -NetworkCategory Private
    }

    # disable IPv6
    Disable-NetAdapterBinding -InterfaceAlias "$($iface.InterfaceAlias)" -ComponentID ms_tcpip6
}

# disable firewall
Set-NetFirewallProfile -All -Enabled "false"